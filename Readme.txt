本程序是参照以下两个网站进行编写
在Python中实现你自己的推荐系统：http://python.jobbole.com/85516/ 
Implementing your own recommender systems in Python：http://online.cambridgecoding.com/notebooks/eWReNYcAfB/implementing-your-own-recommender-systems-in-python-2
主要目的：学习优质代码，充分利用python已有的函数用简洁的语言实现协同过滤，svd
重点学习：pandas numpy scipy
