# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-07-07 17:09:19
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-07-07 18:01:07

import numpy as np
"""
a = np.ones((5,))
print a
print a.shape
b = a[:,np.newaxis]
c = a[np.newaxis,:]
print b
print b+c
print b.shape
"""

"""
c = np.array([[1,2,3],[2,3,4]])
d = np.array([[1.0,2.0]]).T
print c 
print d 
print c/d
"""

x = np.eye(3)
print x
print x.nonzero()
print x[x.nonzero()]