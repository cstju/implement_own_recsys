# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-07-06 16:43:48
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-07-07 20:23:32
import numpy as np
import pandas as pd
from sklearn import cross_validation as cv
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.metrics import mean_squared_error
from math import sqrt
import pdb

data_file = "/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 100K Dataset/ml-100k/u.data"


header  = ['user_id', 'item_id','rating','timestamp']
df = pd.read_csv(data_file, sep = '\t', names = header)

"""
>>> b=np.random.randint(0,5,8)
>>> b
array([2, 3, 3, 0, 1, 4, 2, 4])
>>> np.unique(b)
array([0, 1, 2, 3, 4])(保留数组中不同的值)
>>> c,s=np.unique(b,return_index=True)
>>> c
array([0, 1, 2, 3, 4])
>>> s
array([3, 4, 0, 1, 5])（元素出现的起始位置）
"""

n_users = df.user_id.unique().shape[0]#统计用户数
n_items = df.item_id.unique().shape[0]#统计物品数

print "用户总数为："+str(n_users)
print "物品总数为："+str(n_items)

#按照3：1的比例将数据随机分为训练集和测试集
train_data,test_data = cv.train_test_split(df,test_size = 0.25)

"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
"@@@@@@@@@@下面是协同过滤的代码内容@@@@@@@@@@"
"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

train_data_matrix = np.zeros((n_users,n_items))
for line in train_data.itertuples():
	train_data_matrix[line[1]-1, line[2]-1] = line[3]

test_data_matrix = np.zeros((n_users,n_items))
for line in test_data.itertuples():
	test_data_matrix[line[1]-1, line[2]-1] = line[3]

user_similarity = pairwise_distances(train_data_matrix,metric = 'cosine')#计算两个用户之间的相似度
item_similarity = pairwise_distances(train_data_matrix.T,metric = 'cosine')#计算两个商品之间的相似度

def predict(ratings,similarity,type='user'):
	if type == 'user':
		"""
		axis=1代表是按照行取平均
		mean_user_rating得到的是一个列向量
		"""
		mean_user_rating = ratings.mean(axis = 1)#得到平均值的维度是一维的
		#np.newaxis插入一个新的维度，因为ratings是二维数组，得到的mean_user_rating是一个一维数组，所以插入新的维度
		ratings_diff = ratings - mean_user_rating[:,np.newaxis]
		#得到的pred包含所有评分（训练集+测试集）
		#similarity是一个对称矩阵，所以按照 axis=1行求和 和 按照axis=0列求和 的结果一样
		pred = mean_user_rating[:,np.newaxis]+similarity.dot(ratings_diff)/np.array([np.abs(similarity).sum(axis=1)]).T
	elif type == 'item':
		pred = ratings.dot(similarity)/np.array([np.abs(similarity).sum(axis=1)])
	return pred

item_prediction = predict(train_data_matrix,item_similarity,type='item')
user_prediction = predict(train_data_matrix,user_similarity,type='user')

def rmse(prediction,ground_truth):
	"""
	ground_truth.nonzero()得到的是矩阵ground_truth中非0值得横纵坐标
	>>> x = np.eye(3)
	>>> x
	array([[ 1.,  0.,  0.],
     	  [ 0.,  1.,  0.],
          [ 0.,  0.,  1.]])
	>>> print x.nonzero()
	(array([0, 1, 2]), array([0, 1, 2]))
	>>> print x[x.nonzero()]
	[ 1.  1.  1.]
	"""
	"""
	>>> a = np.array([[1,2], [3,4]])
	>>> a.flatten()#将数组按照行换成1维
	array([1, 2, 3, 4])
	"""
	prediction = prediction[ground_truth.nonzero()].flatten()
	ground_truth = ground_truth[ground_truth.nonzero()].flatten()
	return sqrt(mean_squared_error(prediction,ground_truth))

print "User-based CF RMSE: "+str(rmse(user_prediction,test_data_matrix))
print "Item-based CF RMSE: "+str(rmse(item_prediction,test_data_matrix))

"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
"@@@@@@@@@@@@下面是SVD的代码内容@@@@@@@@@@@@"
"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
"关于svds函数的具体内容看官网：http://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.svds.html"
"要注意有一个参数是which，来决定是按从大到小排序——LM，还是从小到大排序——SM"
import scipy.sparse as sp
from scipy.sparse.linalg import svds
user_feature_matrix,s,item_feature_matrix_T= svds(train_data_matrix, k = 20)
"""
>>> s = np.array([1,2,3])
>>> print np.diag(s)
[[1 0 0]
 [0 2 0]
 [0 0 3]]
"""
s_diag_matrix = np.diag(s)
X_pred = np.dot(np.dot(user_feature_matrix,s_diag_matrix),item_feature_matrix_T)
print "SVD RMSE: "+str(rmse(X_pred,test_data_matrix))



